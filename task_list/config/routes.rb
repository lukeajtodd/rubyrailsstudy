Rails.application.routes.draw do
  get 'home/index'

  resources :tasks

  root 'home#index'
end
