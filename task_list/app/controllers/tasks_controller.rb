class TasksController < ApplicationController
    def show
        @task = Task.find(params[:id])
    end

    def new
        @task = Task.new
    end

    def create
        @task = Task.new(task_params)

        @task.save
        redirect_to @task
    end

    def destroy
        @task = Task.find(params[:id])

        @task.destroy
        redirect_to home_index_path
    end

    private
        def task_params
            params.require(:task).permit(:title, :description)
        end
end
